The Electric Connection of Los Angeles has been providing homeowners and businesses alike with expert electrical services for over 25 Years. We've built a company of highly experienced, certified electricians to serve your electrical needs. We'll provide you with quality work at honest prices.

Address: 2706 S Robertson Blvd, Los Angeles, CA 90034, USA

Phone: 213-401-2334
